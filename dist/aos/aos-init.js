// Set animation state
const animation_toggle = document.createElement("button");
const body = document.querySelector("body");
const media_query = window.matchMedia("(prefers-reduced-motion: reduce)");

animation_toggle.innerHTML = `<span class="button-icon" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" height="12px" viewBox="0 0 24 24" width="12px" fill="#000000"><path d="M0 0h24v24H0z" fill="none"/><path d="M6 19h4V5H6v14zm8-14v14h4V5h-4z"/></svg></span> Disable Animations`;
animation_toggle.classList.add("toggle-animation");
document.querySelector(".main-menu").append(animation_toggle);

function animations_on() {
  AOS.init({
    duration: 1000,
    once: true,
    initClassName: 'aos-init',
    anchorPlacement: 'top-bottom'
  });

  animation_toggle.classList.remove('animations-off');
  animation_toggle.classList.add('animations-on');
  body.classList.remove('animations-off');
  body.classList.add('animations-on');
  animation_toggle.innerHTML = `<span class="button-icon" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" height="12px" viewBox="0 0 24 24" width="12px" fill="#000000"><path d="M0 0h24v24H0z" fill="none"/><path d="M6 19h4V5H6v14zm8-14v14h4V5h-4z"/></svg></span> Disable Animations`;

  try {
    localStorage.setItem('animation_preference', 'on');    
  } catch (e) {
    consolelog('Could not set local storage');
  }
}

function animations_off() {
  AOS.init({
    disable: true
  });

  animation_toggle.classList.remove('animations-on');
  animation_toggle.classList.add('animations-off');
  body.classList.remove('animations-on');
  body.classList.add('animations-off');
  animation_toggle.innerHTML = `<span class="button-icon" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" height="12px" viewBox="0 0 24 24" width="12px" fill="#000000"><path d="M0 0h24v24H0z" fill="none"/><path d="M8 5v14l11-7z"/></svg></span> Enable Animations <span class="visually-hidden" role="presentation">, reloads page</span>`;

  try {
    localStorage.setItem('animation_preference', 'off');
  } catch (e) {
    consolelog('Could not set local storage');
  }
}

/* Use any existing preference -----------------------------------------------*/
if (localStorage.getItem('animation_preference')) {
  if (localStorage.getItem('animation_preference') == 'on') {
    animations_on();
  }
  else {
    animations_off();
  }
} /* Otherwise use system setting --------------------------------------------*/
else if (!media_query || media_query.matches) {
  animations_off();
} /* Otherwise default to on -------------------------------------------------*/
else {
  animations_on();
}


/* Toggle setting ------------------------------------------------------------*/
animation_toggle.addEventListener('click', (event) => {
  if (animation_toggle.classList.contains('animations-off')) {
    animations_on();
    window.location.reload();
  } else {
    animations_off()
  }
});