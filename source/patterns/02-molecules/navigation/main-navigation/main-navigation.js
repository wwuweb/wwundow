const main_menu = document.querySelector(".main-menu");
const toggle = document.querySelector(".toggle-menu");
const toggle_icon = document.querySelector(".toggle-menu-icon");
const toggle_text = document.querySelector(".toggle-menu-text");
const menu_items = document.querySelectorAll(".main-menu a, .main-menu button");

function toggle_menu() {
    if (main_menu.classList.contains("menu--closed")) {
        open_menu();
    } else {
        close_menu();
    }
}

function open_menu() {
    toggle.setAttribute("aria-expanded", "true");
    toggle_icon.innerHTML= `<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></svg>`;
    toggle_text.innerHTML = `Close <span class="visually-hidden">Navigation Menu</span>`;       
    main_menu.setAttribute("aria-hidden", "false");
    main_menu.classList.remove("menu--closed");      
    for (const item of menu_items) {
        item.removeAttribute("tabindex");
    }      
}

function close_menu() {
    toggle.setAttribute("aria-expanded", "false");
    toggle_icon.innerHTML= `<svg xmlns="http://www.w3.org/2000/svg" height="80px" viewbox="0 0 24 24" width="80px"><path d="M0 0h24v24H0z" fill="none"/><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/></svg>`;
    toggle_text.innerHTML = `Open <span class="visually-hidden">Navigation Menu</span>`;   
    main_menu.setAttribute("aria-hidden", "true");    
    main_menu.classList.add("menu--closed");   
    for (const item of menu_items) {
        item.setAttribute("tabindex", "-1");
    }
}

toggle.addEventListener("click", toggle_menu);
close_menu();
