var page_links = document.querySelectorAll('.block--page-headings-menu-page-heading-menu .field-content a');
const main_menu = document.querySelector(".main-menu .menu");
const first_banner = document.querySelector(".paragraph--type--featured-story");
const page_menu_heading = document.createElement("h2");
const page_menu = document.createElement("ul");
const page_nav = document.createElement("nav");

page_menu_heading.innerText = "On this Page";
page_menu_heading.classList.add("visually-hidden");
page_menu_heading.setAttribute("id", "page-menu");
page_menu.classList.add("page-menu");

for (const item of page_links) {
    var li = document.createElement("li");
    li.appendChild(item);
    page_menu.appendChild(li);
}

/* Add to main menu */
main_menu.after(page_menu);
main_menu.after(page_menu_heading);

/* Add to content area */
first_banner.after(page_nav);
page_nav.setAttribute("aria-labelledby", "page-menu"); 
page_nav.appendChild(page_menu_heading.cloneNode(true));
page_nav.appendChild(page_menu.cloneNode(true));
