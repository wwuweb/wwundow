try {
    var odd_photo_rows = document.querySelector('.block--story-blocks-odd-photos').querySelectorAll('.views-row');
    var even_quotes = document.querySelector('.block--story-blocks-even-quotes').querySelectorAll('.views-field-field-pull-quotes');
    
    for (let i = 0; i < even_quotes.length; i++ ) {
        let placed = false;    
        
        for (let j = 0; j < odd_photo_rows.length; j++ ) {
            if (odd_photo_rows[j].innerHTML === "" && !placed) {        
                odd_photo_rows[j].appendChild(even_quotes[i]);
                placed = true;
            }   
        }    
    }
}
catch(err) {
    console.log("no story blocks");
}



try {
    var even_photo_rows = document.querySelector('.block--story-blocks-even-photos').querySelectorAll('.views-row');
    var odd_quotes = document.querySelector('.block--story-blocks-odd-quotes').querySelectorAll('.views-field-field-pull-quotes');
    
    for (let i = 0; i < odd_quotes.length; i++ ) {
        let placed = false;
        
        for (let j = 0; j < even_photo_rows.length; j++ ) {
            if (even_photo_rows[j].innerHTML === "" && !placed) {  
                even_photo_rows[j].appendChild(odd_quotes[i]);
                placed = true;
            }   
        }    
    }
}
catch(err) {
    console.log("no story blocks");
}